import Vue from 'vue'
import VueAxios from 'vue-axios'
import axios from 'axios'
import App from './App.vue'
import VueMaterial from 'vue-material'
import router from './router'
import Restricted from './layouts/Restricted'
import Unrestricted from './layouts/Unrestricted'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import Configs from './configs/constants'
const _json = require('querystring')

Object.defineProperty(Vue.prototype, '$_json', { value: _json });


Vue.component('app-restricted-layout', Restricted);
Vue.component('app-unrestricted-layout', Unrestricted);

import 'vue-material/dist/vue-material.min.css'


import { MdButton, MdContent, MdTabs } from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false

Vue.use(
  VueMaterial,
  VueAxios,
  axios,
  MdButton,
  MdContent,
  MdTabs,
  BootstrapVue,
  IconsPlugin,
  ),

  Vue.mixin({
    data() {
      return {
        config: Configs
      }
    }, 
    methods: {
      getToken() {
        return localStorage.getItem('access_token')
      },
      goToIntrospect: async function() {
        let headers = {
          'Content-Type': this.config.Authorization.jsonEncoded,
          'Authorization': this.config.Authorization.basicSchema + this.config.Authorization.introSpect
        }
  
        let params = {
          'token': this.getToken()
        }
        
      return await axios.post(this.config.Authorization.url + '/connect/introspect' + this.$_json.stringify(params),{
           headers : headers
        })
      }
    }
  })

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')