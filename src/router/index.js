import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)


const routes = [ 
    {
        path: '*',
        redirect: {
            name: "Login"
        },
        component: () => import('../components/unrestricted/Login.vue'),
        meta: {
            layout: 'app-unrestricted-layout',
            requiresAuth: false
        }
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import('../components/unrestricted/Login.vue'),
        meta: {
            layout: 'app-unrestricted-layout',
            requiresAuth: false
        }
    },
    {
        path: '/forget-password',
        name: 'ForgetPassword',
        component: () => import('../components/unrestricted/ForgetPassword.vue'),
        meta: {
            layout: 'app-unrestricted-layout',
            requiresAuth: false
        }
    },
    {
        path: '/reset-password',
        name: 'SetPassword',
        component: () => import('../components/unrestricted/SetPassword.vue'),
        meta: {
            layout: 'app-unrestricted-layout',
            requiresAuth: false
        }
    },
    {
        path: '/email-sent',
        name: 'EmailSent',
        component: () => import('../components/unrestricted/EmailSent.vue'),
        meta: {
            layout: 'app-unrestricted-layout',
            requiresAuth: false
        }
    },
    {
        path: '/dashboard',
        name: 'Dashboard',
        component: () => import('../components/restricted/Dashboard.vue'),
        meta: {
            layout: 'app-restricted-layout',
            requiresAuth: true
        }
    },
    {
        path: '/profile',
        name: 'Profile',
        component: () => import('../components/restricted/Profile.vue'),
        meta: {
            layout: 'app-restricted-layout',
            requiresAuth: true
        }
    },
    {
        path: '/change-password',
        name: 'ChangePassword',
        component: () => import('../components/restricted/ChangePassword.vue'),
        meta: {
            layout: 'app-restricted-layout',
            requiresAuth: true
        }
    },
    {
        path: '/register-user',
        name: 'RegisterUser',
        component: () => import('../components/restricted/RegisterUser.vue'),
        meta: {
            layout: 'app-restricted-layout',
            requiresAuth: true
        }
    },
    {
        path: '/users',
        name: 'Users',
        component: () => import('../components/restricted/Users.vue'),
        meta: {
            layout: 'app-restricted-layout',
            requiresAuth: true
        }
    },
    {
        path: '/edit-profile',
        name: 'EditProfile',
        component: () => import('../components/restricted/EditProfile.vue'),
        meta: {
            layout: 'app-restricted-layout',
            requiresAuth: true
        }
    },
    // {
    //     path: '/projects',
    //     name: 'AddProject',
    //     component: () => import('../components/restricted/Projects/AddProject.vue'),
    //     meta: {
    //         layout: 'app-restricted-layout',
    //         requiresAuth: true
    //     }
    // },
    {
        path: '/projects',
        name: 'Projects',
        props: true,
        component: () => import('../components/restricted/Projects/Projects.vue'),
        meta: {
            layout: 'app-restricted-layout',
            requiresAuth: true
        }
    },
    {
        path: '/microservices',
        name: 'Microservices',
        props: true,
        component: () => import('../components/restricted/Microservices/Microservices.vue'),
        meta: {
            layout: 'app-restricted-layout',
            requiresAuth: true
        }
    },
    {
        path: '/microservices/api',
        name: 'MicroservicesApi',
        props: true,
        component: () => import('../components/restricted/Api/MicroservicesApi.vue'),
        meta: {
            layout: 'app-restricted-layout',
            requiresAuth: true
        }
    }
]

 

const router = new VueRouter({
    mode: 'history',
    routes
})

function getAccesToken() {
    return localStorage.getItem('access_token');
}




router.beforeEach((to, from, next) => {

    if (to.matched.some((record) => record.meta.requiresAuth == true)) {
      if(getAccesToken() != null){
        next()
      }else {
        next('/login')
      }
    }else {
        next()
        if(getAccesToken() != null){
            next('/dashboard')
          }else {
            next()
          }
    }
  })

   


export default router